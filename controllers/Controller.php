<?php

/**
 * Created by PhpStorm.
 * User: renato
 * Date: 27.11.16
 * Time: 0:04
 */
namespace artj\controllers;

use \artj\models\Model;

class Controller
{
    public $model;
    public $view;

    public function __construct()
    {
        $this->model = new Model();
    }

    public function actionIndex()
    {
        $regions = $this->model->selectAllRegions();
        require __DIR__ . '/../web/index.php';
    }

    /**
     * @param $location
     * @return string
     */
    public function actionCities($location)
    {
        $result = $this->model->selectCitiesByRegion($location);
        if ($result)
            return json_encode($result);
        return json_encode($this->model->selectCitiesByRegion($location, 3));
    }

    /**
     * @param $location
     * @return string
     */
    public function actionDistricts($ter_pid)
    {
        return json_encode($this->model->selectDistrictsByTerId($ter_pid));
    }

    /**
     * @param $username
     * @param $email
     * @param $password
     * @return string
     */
    public function createUser($username, $email, $password)
    {
        return json_encode($this->model->createUser($username, $email, $password));
    }
}