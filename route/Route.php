<?php

/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.11.16
 * Time: 23:56
 */
namespace artj\route;

use artj\controllers\Controller;

class Route
{
    static function execute()
    {
        $controller = new Controller();

        if(isset($_POST['location'])){
           echo $controller->actionCities(addslashes($_POST['location']));
            die();
        }

        if(isset($_POST['ter_pid'])){
            echo $controller->actionDistricts(addslashes($_POST['ter_pid']));
            die();
        }

        if(isset($_POST['user_name']) && isset($_POST['user_email']) && isset($_POST['user_location_id'])){
            echo $controller->createUser(addslashes($_POST['user_name']), addslashes($_POST['user_email']), addslashes($_POST['user_location_id']));
            die();
        }

        $controller->actionIndex();
    }
}