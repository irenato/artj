<?php

/**
 * Created by PhpStorm.
 * User: renato
 * Date: 27.11.16
 * Time: 0:02
 */
namespace artj\models;

use \PDO;

class Model
{
    private $pdo;


    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        $this->pdo->exec("set names utf8");
    }

    /**
     * @return array
     */
    public function selectAllRegions()
    {
        $results = $this->pdo->query('SELECT ter_id, ter_name, reg_id FROM t_koatuu_tree WHERE ter_pid IS NULL');
        return $results->fetchAll();
    }

    /**
     * @param $region
     * @return string
     */
    public function selectCitiesByRegion($region, $ter_type_id = 1)
    {
        $results = $this->pdo->query('SELECT ter_id, ter_name, ter_pid FROM t_koatuu_tree WHERE reg_id=' . $region . ' AND ter_type_id=' . $ter_type_id);
        return $results->fetchAll();
    }

    /**
     * @param $ter_id
     * @return string
     */
    public function selectDistrictsByTerId($ter_id)
    {
        $results = $this->pdo->query('SELECT ter_address, ter_id FROM t_koatuu_tree WHERE ter_pid=' . $ter_id . ' AND ter_type_id=2');
        return $results->fetchAll();
    }

    /**
     * @param $username
     * @param $email
     * @param $location_id
     * @return bool
     */
    public function createUser($username, $email, $location_id)
    {
        $user = $this->checkUser($email);
        if(!empty($user)){
            $user->status = 'exists';
            return $user;
            die();
        }
        $sql = 'INSERT INTO users (username, email, t_koatuu_tree_id) VALUES (:username, :email, :t_koatuu_tree_id)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':t_koatuu_tree_id', $location_id, PDO::PARAM_INT);
        if ($stmt->execute())
            return $this->selectUser($this->pdo->lastInsertId());
    }

    /**
     * @param $email
     * @return mixed
     */
    private function checkUser($email){
        $results = $this->pdo->prepare('SELECT users.username, users.email, users.t_koatuu_tree_id , t_koatuu_tree.ter_address FROM users, t_koatuu_tree WHERE users.email=? AND `users`.`t_koatuu_tree_id`=`t_koatuu_tree`.`ter_id`');
        $results->execute(array($email));
        return $results->fetchObject();
    }

    /**
     * @param $id
     * @return mixed
     */
    private function selectUser($id){
        $results = $this->pdo->prepare('SELECT users.username, users.email, users.t_koatuu_tree_id , t_koatuu_tree.ter_address FROM users, t_koatuu_tree WHERE users.id=? AND `users`.`t_koatuu_tree_id`=`t_koatuu_tree`.`ter_id`');
        $results->execute(array($id));
        return $results->fetchObject();
    }
}