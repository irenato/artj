<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TEST</title>

    <!-- Bootstrap -->
    <link href="web/css/bootstrap.min.css" rel="stylesheet">
    <link href="web/css/chosen.css" rel="stylesheet">
    <link href="web/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="confirmation">
                    <div class="description">
                        <h2 class="title">Тестовое задание</h2>
                        <form>
                            <div class="form-group">
                                <input type="text" id="artj-fullname" class="form-control" placeholder="Ф.И.О.">
                            </div>
                            <div class="form-group">
                                <input type="text" id="artj-email" class="form-control" placeholder="email">
                                <p class="error-messages">Nome non valido</p>
                            </div>
                            <input type="hidden" id="artj-user-location">
                            <div class="form-group">
                                <select data-placeholder="Выберите область..." id="arj-location" style="width:300px;"
                                        class="chosen-select">
                                    <?php foreach ($regions as $region) : ?>
                                        <option value="<?= $region['reg_id'] ?>"><?= $region['ter_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <span class="text-danger error-message" style="display: none">Выберите местоположение</span>
                            <div class="form-group" id="artj-cities" style="display: none">
                                <select data-placeholder="Выберите город..." style="width:300px;">
                                </select>

                            </div>

                            <div class="form-group" id="artj-districts" style="display: none">
                                <select data-placeholder="Выберите район..." style="width:300px;">
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Сохранить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="modal fade" id="about-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <ul class="list-group">

                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="web/js/jquery-2.1.4.min.js"></script>
    <script src="web/js/chosen.jquery.js"></script>
    <script src="web/js/bootstrap.min.js"></script>
    <script src="web/js/common.js"></script>
</footer>
</body>
</html>
