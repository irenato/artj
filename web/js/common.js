$(document).ready(function () {
    $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});

    $('#arj-location').on('change', function () {
        $.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                'location': $(this).val(),
            },
            success: showCitiesBlock,
        });

    });

    $(document).on('change', '#artj-cities select', function () {
        addUserLocationId($(this).find(':selected').attr('data-location_id'));
        selectDistricts($(this).val());
    });

    $(document).on('change', '#artj-districts select', function () {
        addUserLocationId($(this).val());
    });

    $('form .btn').click(function (e) {
        e.preventDefault();
        emailValid($('#artj-email'));
        nameValid($('#artj-fullname'));
        if ($('#artj-cities').is(':hidden')) {
            $(this).closest('form').find('select').addClass('error');
            $('span.error-message').show();
        } else {
            $(this).closest('form').find('select').removeClass('error');
            $('span.error-message').hide();
        }

        if ($(this).closest('form').find('.error').length < 1) {
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    'user_name': $('#artj-fullname').val(),
                    'user_email': $('#artj-email').val(),
                    'user_location_id': $('#artj-user-location').val(),
                },
                success: showResult,
            });
        }

    });

    $('#about-user').on('hidden.bs.modal', function () {
        $('#about-user ul.list-group li').detach();
        $('#myModalLabel').text('')
    })

    function showResult(result) {
        var userdata = JSON.parse(result),
            str = '',
            message = typeof userdata.status == 'undefined' ? 'Пользователь создан!' : 'Пользователь существует!';

        str += '<li class=list-group-item>' + userdata.username + '</li>';
        str += '<li class=list-group-item>' + userdata.email + '</li>';
        str += '<li class=list-group-item>' + userdata.ter_address + '</li>';

        $('#myModalLabel').text(message)
        $('#about-user ul.list-group').append(str);
        $('#about-user').modal('show');

    }

    function emailValid(email_input) {
        var rv_mail = /.+@.+\..+/i;

        if ($(email_input).val().length > 1 && rv_mail.test($(email_input).val()))
            $(email_input).removeClass('error').addClass('success');
        else
            $(email_input).removeClass('success').addClass('error');

    }

    function nameValid(full_name_input) {
        if ($(full_name_input).val().length > 3)
            $(full_name_input).removeClass('error').addClass('success');
        else
            $(full_name_input).removeClass('success').addClass('error');
    }

    function showCitiesBlock(result) {
        var cities = JSON.parse(result),
            str = '';
        for (i = 0; i < cities.length; i++) {
            str += '<option value="' + cities[i].ter_pid + '" data-location_id="' + cities[i].ter_id + '">' + cities[i].ter_name + '</option>';
        }
        addUserLocationId(cities[0].ter_id);
        $('#artj-cities select').empty().append(str);
        selectDistricts(cities[0].ter_pid);
        $('#artj-cities').show();
    }

    function showDistrictsBlock(result) {
        var cities = JSON.parse(result),
            str = '';
        if (cities.length > 0) {
            for (i = 0; i < cities.length; i++) {
                str += '<option value="' + cities[i].ter_id + '">' + cities[i].ter_address + '</option>';
            }
            addUserLocationId(cities[0].ter_id);
            $('#artj-districts select').empty().append(str);
            $('#artj-districts').show();
        } else {
            $('#artj-districts select').empty();
            $('#artj-districts').hide();
        }

    }

    function selectDistricts(data) {
        $.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                'ter_pid': data,
            },
            success: showDistrictsBlock,
        });
    }

    function addUserLocationId(location_id) {
        if (location_id)
            $('#artj-user-location').val(location_id);
    }
});
