<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.11.16
 * Time: 23:52
 */

ini_set('display_errors', 1);

/**
 * db settings
 */
define('DB_NAME', 'artj');
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');

/**
 * @param $class
 */
function __autoload($class)
{
    require str_replace("\\", '/', $class) . '.php';
}